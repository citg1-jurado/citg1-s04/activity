package com.zuitt;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	
	public void init() throws ServletException {
			
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
//		a. First name - System Properties
		String firstname = req.getParameter("firstname");
		System.getProperties().put("firstname", firstname); //System Properties
		
//		b. Last name - HttpSession
		String lastname = req.getParameter("lastname");
		HttpSession session = req.getSession();		//HttpSession
		session.setAttribute("lastname", lastname);	//HttpSession
		
//		c. Email - Servlet Context setAttribute method
		String email = req.getParameter("email");
		ServletContext srvContext = getServletContext(); //Servlet Context
		srvContext.setAttribute("email", email);		 //Servlet Context
		
//		d. Contact Number - Url Rewriting via sendRedirect method
		RequestDispatcher rd = req.getRequestDispatcher("details");	//Hindi ko pa rin gets :(
		rd.forward(req, res);
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroy. ");
		System.out.println("******************************************");
	}

}
